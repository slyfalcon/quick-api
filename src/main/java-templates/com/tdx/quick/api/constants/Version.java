package com.tdx.quick.api.constants;

public final class Version {
    /**
     * project version
     */
    public static final String VERSION = "${project.version}";

    /**
     * build timestamp
     */
    public static final String TIMESTAMP = "${timestamp}";
}