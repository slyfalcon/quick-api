@echo off

set serverPort=7700
set logCharset=gbk
set logDir=%~dp0\..\logs
set appName=QuickApi
set vmArgs=-Xms128m -Xmx512m

REM ------ default set ------
set clientTag=quick_api_%serverPort%
set mainClass=com.tdx.quick.api.QuickApiApplication
set javaArgs=-Dfile.encoding=UTF-8 -Dserver.port=%serverPort%
set configDir=conf
set baseDir=%~dp0\..

set line=-----------------------------------------------

title %appName% %serverPort% [%date% %time%]

set classPath=%baseDir%\;%baseDir%\%configDir%;%baseDir%\lib\*

java %javaArgs% %vmArgs% -cp %classPath% %mainClass% %clientTag%

pause