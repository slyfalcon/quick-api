package com.tdx.quick.api;

import com.tdx.quick.api.constants.Version;
import lombok.extern.slf4j.Slf4j;
import net.hasor.spring.boot.EnableHasor;
import net.hasor.spring.boot.EnableHasorWeb;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author tdx.lq
 * @since 2020-12-11 23:31
 */
@EnableHasor()
@EnableHasorWeb()
@SpringBootApplication
@Slf4j
public class QuickApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(QuickApiApplication.class, args);
        log.warn("version:{}, build time:{}", Version.VERSION, Version.TIMESTAMP);
    }
}
