package com.tdx.quick.api.config;

import org.springframework.boot.autoconfigure.orm.jpa.HibernateProperties;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateSettings;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Jpa 数据对象自动映射表配置
 *
 * @author tdx.lq
 * @since 2020/11/4 19:15
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        // repository包名
        basePackages = JpaConf.JPA_PACKAGES,
        // 实体管理bean名称
        entityManagerFactoryRef = "baseEntityManagerFactory",
        // 事务管理bean名称
        transactionManagerRef = "baseTransactionManager"
)
public class JpaConf {
    public static final String JPA_PACKAGES = "com.tdx.quick.api.core";

    @Resource(name = "baseDataSource")
    private DataSource baseDataSource;

    /**
     * 生成配置文件，包括 JPA配置文件和Hibernate配置文件，相当于以下三个配置
     * spring.jpa.show-sql=false
     * spring.jpa.open-in-view=false
     * spring.jpa.hibernate.ddl-auto=update
     *
     * @return 配置 Map
     */
    private static Map<String, Object> genDataSourceProperties() {
        JpaProperties jpaProperties = new JpaProperties();
        jpaProperties.setOpenInView(false);
        jpaProperties.setShowSql(false);
        jpaProperties.setProperties(new HashMap<String, String>() {
            {
                put("hibernate.jdbc.time_zone", "Asia/Shanghai");
            }
        });

        HibernateProperties hibernateProperties = new HibernateProperties();
        hibernateProperties.setDdlAuto("update");

        // 配置JPA自定义表名称策略
        hibernateProperties.getNaming().setPhysicalStrategy(JpaPhysicalNamingStrategy.class.getName());
        HibernateSettings hibernateSettings = new HibernateSettings();
        return hibernateProperties.determineHibernateProperties(jpaProperties.getProperties(), hibernateSettings);
    }

    @Primary
    @Bean(name = "baseEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean initBaseEntityManagerFactory(EntityManagerFactoryBuilder builder) {

        return builder
                .dataSource(baseDataSource)
                .properties(genDataSourceProperties())
                .packages(JPA_PACKAGES)
                .persistenceUnit("corePersistenceUnit")
                .build();
    }

    @Primary
    @Bean(name = "baseTransactionManager")
    public PlatformTransactionManager initBaseTransactionManager(EntityManagerFactoryBuilder builder) {
        return new JpaTransactionManager(Objects.requireNonNull(initBaseEntityManagerFactory(builder).getObject()));
    }
}
