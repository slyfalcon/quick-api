package com.tdx.quick.api.config;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
import org.springframework.util.StringUtils;

import java.io.Serializable;

/**
 * 自动生成表策略
 */
public class JpaPhysicalNamingStrategy extends SpringPhysicalNamingStrategy implements Serializable {
    /**
     * 映射物理表名称，如：把实体表 AppInfoDO 的 DO 去掉
     *
     * @param name        实体名称
     * @param environment 环境变量
     * @return 映射后的物理表
     */
    @Override
    public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment environment) {
        String text = name.getText();
        String noDOText = StringUtils.endsWithIgnoreCase(text, "do") ? text.substring(0, text.length() - 2) : text;
        return super.toPhysicalTableName(new Identifier(noDOText, name.isQuoted()), environment);
    }

}
