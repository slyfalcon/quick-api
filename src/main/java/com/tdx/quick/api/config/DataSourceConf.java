package com.tdx.quick.api.config;

import com.tdx.quick.api.utils.DBUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * 基础数据源配置
 *
 * @author tdx.lq
 * @since 2020/11/4 19:20
 */
@Configuration
@Slf4j
public class DataSourceConf {

    @Value("${basePool}")
    private String basePool;

    @Primary
    @Bean("baseDataSource")
    public DataSource baseDataSource() {
        log.info("base pool is {}", basePool);
        return DBUtils.getDataSource(basePool);
    }

}
