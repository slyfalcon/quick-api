package com.tdx.quick.api.controller;

import com.tdx.quick.api.constants.Version;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 默认控制器
 *
 * @author tdx.lq
 * @since 2020-12-12 0:50
 */
@Controller
public class DefaultController {
    @RequestMapping(value = {"/", "/index.html"})
    public String index() {
        return "redirect:/console/quick_api/#/?ver=" + Version.VERSION
                + "&ts=" + Version.TIMESTAMP;
    }
}
