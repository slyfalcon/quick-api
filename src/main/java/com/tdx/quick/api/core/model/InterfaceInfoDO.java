package com.tdx.quick.api.core.model;

import lombok.Data;

import javax.persistence.*;

/**
 * API 信息表
 *
 * @author tdx.lq
 * @since 2020/9/25 0:21
 */

@Data
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(name = "uk_interface_info", columnNames = {"apiPath"})})
public class InterfaceInfoDO {

    @Id
    @Column(nullable = false, columnDefinition = "varchar(64) comment 'ID'")
    private String apiId;

    @Column(nullable = false, columnDefinition = "varchar(12) comment 'HttpMethod：GET、PUT、POST'")
    private String apiMethod;

    @Column(nullable = false, columnDefinition = "varchar(128) comment '拦截路径'")
    private String apiPath;

    @Column(nullable = false, columnDefinition = "varchar(4) comment '状态：-1-删除, 0-草稿，1-发布，2-有变更，3-禁用'")
    private String apiStatus;

    @Column(nullable = false, columnDefinition = "varchar(255) comment '注释'")
    private String apiComment;

    @Column(nullable = false, columnDefinition = "varchar(24) comment '脚本类型：SQL、DataQL'")
    private String apiType;

    @Column(nullable = false, columnDefinition = "mediumtext comment '查询脚本'")
    private String apiScript;

    @Column(nullable = false, columnDefinition = "mediumtext comment '接口的请求/响应数据结构'")
    private String apiSchema;

    @Column(nullable = false, columnDefinition = "mediumtext comment '请求/响应/请求头样本数据'")
    private String apiSample;

    @Column(nullable = false, columnDefinition = "mediumtext comment '扩展配置信息'")
    private String apiOption;

    @Column(nullable = false, columnDefinition = "varchar(32) comment '创建时间'")
    private String apiCreateTime;

    @Column(nullable = false, columnDefinition = "varchar(32) comment '修改时间'")
    private String apiGmtTime;

}
