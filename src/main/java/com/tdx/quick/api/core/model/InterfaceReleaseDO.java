package com.tdx.quick.api.core.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * API 发布表
 *
 * @author tdx.lq
 * @since 2020/9/25 0:21
 */

@Data
@Entity
@Table(indexes = {
        @Index(name = "idx_interface_release_api", columnList = "pubApiId"),
        @Index(name = "idx_interface_release_path", columnList = "pubPath"),
})
public class InterfaceReleaseDO implements Serializable {
    private static final long serialVersionUID = 1610422092512350898L;

    @Id
    @Column(nullable = false, columnDefinition = "varchar(64) comment 'Publish ID'")
    private String pubId;

    @Column(nullable = false, columnDefinition = "varchar(64) comment '所属 API ID'")
    private String pubApiId;

    @Column(nullable = false, columnDefinition = "varchar(12) comment 'HttpMethod：GET、PUT、POST'")
    private String pubMethod;

    @Column(nullable = false, columnDefinition = "varchar(128) comment '拦截路径'")
    private String pubPath;

    @Column(nullable = false, columnDefinition = "varchar(4) comment '状态：-1-删除, 0-草稿，1-发布，2-有变更，3-禁用'")
    private String pubStatus;

    @Column(nullable = false, columnDefinition = "varchar(255) comment '注释'")
    private String pubComment;

    @Column(nullable = false, columnDefinition = "varchar(24) comment '脚本类型：SQL、DataQL'")
    private String pubType;

    @Column(nullable = false, columnDefinition = "mediumtext comment '查询脚本'")
    private String pubScript;

    @Column(nullable = false, columnDefinition = "mediumtext comment '原始查询脚本，仅当类型为SQL时'")
    private String pubScriptOri;

    @Column(nullable = false, columnDefinition = "mediumtext comment '接口的请求/响应数据结构'")
    private String pubSchema;

    @Column(nullable = false, columnDefinition = "mediumtext comment '请求/响应/请求头样本数据'")
    private String pubSample;

    @Column(nullable = false, columnDefinition = "mediumtext comment '扩展配置信息'")
    private String pubOption;

    @Column(nullable = false, columnDefinition = "varchar(32) comment '发布时间（下线不更新）'")
    private String pubReleaseTime;

}
