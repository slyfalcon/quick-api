# 快速 API 服务
用于快速启动一个可在线编辑的 API 服务

## 动态数据源
- sql 语句中使用 `/*@@数据源名称*/` 即可

## JavaScript 支持
```
var jsCall = @@js(msg)<%
    return 'from js:' + msg
%>
```

## Python 支持
```
var pyCall = @@python(msg)<%
    return 'from python:' + msg
%>
```

## 功能介绍
- 支持自定义 API 路径
- 支持混合数据源的查询
- 内置翻页函数
- 支持 JS 扩展（es6 完整支持）
- 支持 python 扩展（2.7.3 稳定版）
- 支持 neo4j 数据源
- 支持 http 扩展
- 支持 sql 扩展
- 支持内部子 api 调用

## 版本记录
- 2021.03.09 v1.0.2 增加 neo4j 的支持
- 2020.12.12 v1.0.0 发布